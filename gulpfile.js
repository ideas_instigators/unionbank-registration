var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

var jsFiles = ['*.js', 'src/**/*.js'];

gulp.task('style', function(){
    gulp.src(jsFiles);
})

gulp.task('serve', ['style'], function(){
    var options = {
        script: 'app.js',
        delayTime: 1,
        env: {
            'PORT': 3000
        },
        watch: jsFiles
    }
    
    return nodemon(options)
        .on('restart', function(ev){
            console.log('Restarting...');
        });
});