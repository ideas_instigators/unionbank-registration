var router = require('express').Router();

router.use('/', require('./pages/index'));
router.use('/api', require('./api/index'));

module.exports = router; 