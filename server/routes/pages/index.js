
var request = require('request'),
  router = require('express').Router(),
  registrantService = require('../../db/service/registrant');

router.get('/', function (req, res, next) {
  res.render('registration');
});

router.post('/', function (req, res, next) {

  // if (req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
  //     return res.json({ "responseCode": 1, "responseDesc": "Please select captcha" });
  // }

  // var secretKey = "6LeMlhEUAAAAAIC0O-i4_eff-AZY4soxp9vK750D";

  // var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey +
  //     "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;

  // request(verificationUrl, function (error, response, body) {
  //     body = JSON.parse(body);
  //     if (body.success !== undefined && !body.success) {
  //         return res.json({ "responseCode": 1, "responseDesc": "Failed captcha verification" });
  //     }


  // });

  let data = {};

  data.firstName = req.body.first_name;
  data.lastName = req.body.last_name;
  data.email = req.body.email;
  data.mobileNumber = req.body.mobile_number;
  data.department = req.body.department;
  data.branch = req.body.branch;

  return registrantService.addRegistrant(data)
    .then(result => {
      res.redirect('/success');
    })
    .catch(err => console.log(err));
});

router.get('/success', function (req, res, next) {
  res.render('registration_success');
});

router.get('/udx/checkin', function (req, res, next) {
  res.render('checkin_admin');
});

router.get('/uhac/checkin', function (req, res, next) {
  res.render('checkin_admin_uhac');
})

module.exports = router;
