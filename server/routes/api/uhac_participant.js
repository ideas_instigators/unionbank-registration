'use strict'

var router = require('express').Router(),
    participantService = require('../../db/service/uhac_participant');

router.post('/', function (req, res, next) {
    let data = {};

    data.firstName  = req.body.first_name;
    data.lastName   = req.body.last_name;
    data.email      = req.body.email;
    data.mobileNumber = req.body.mobile_number;
    data.age = req.body.age;
    data.organization = req.body.organization;
    data.position = req.body.position;
    data.skill_type = req.body.skill_type;
    data.team = req.body.team;

    return participantService.addParticipant(data)
        .then(result => {
            res.status(200).send({ "message": "okay" });
        })
        .catch(err => console.log(err));
});

router.put('/checkin/:id', function(req, res, next) {
    let id = req.params.id || null,
        isCheckin = req.query.checkin || true;
    
    console.log(isCheckin);
    
    if(id) {
        return participantService.updateUhacCheckin(id, isCheckin)
            .then(() => {
                res.end(JSON.stringify({status: 'success'}));
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({error: "Bad Request"});
            })
    }
});

router.get('/participants', function(req, res, next){
    return participantService.getParticipant()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: "Internal Server Error"})
        });
})

module.exports = router;