'use strict'

var router = require('express').Router(),
    registrantService = require('../../db/service/registrant');

router.post('/', function (req, res, next) {
    let data = {};

    data.firstName  = req.body.first_name;
    data.lastName   = req.body.last_name;
    data.email      = req.body.email;
    data.mobileNumber = req.body.mobile_number;
    data.department = req.body.department;
    data.branch     = req.body.branch;

    return registrantService.addRegistrant(data)
        .then(result => {
            res.status(200).send({ "message": "okay" });
        })
        .catch(err => console.log(err));
});

router.put('/checkin/:id', function(req, res, next) {
    let id = req.params.id || null,
        isCheckin = req.query.checkin || true;
    
    console.log(isCheckin);
    
    if(id) {
        return registrantService.updateRegistrantCheckin(id, isCheckin)
            .then(() => {
                res.end(JSON.stringify({status: 'success'}));
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({error: "Bad Request"});
            })
    }
});

router.get('/registrants', function(req, res, next){
    return registrantService.getRegistrant()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: "Internal Server Error"})
        });
})

module.exports = router;