var router = require('express').Router();

router.use('/registration', require('./registration'));
router.use('/uhac', require('./uhac_participant'));

module.exports = router;
