'use strict'

var db = require("../db");

const registrantTbl = 'ub_registrant';

var upsertRegistrant = (data) => {

    var registrantRepo = new (require('./generic').Generic)(registrantTbl);

    return registrantRepo.upsertUsingMultiFields({
        first_name: data.firstName,
        last_name: data.lastName,
        email: data.email,
        mobile_number: data.mobileNumber,
        department: data.department,
        branch: data.branch
    }, {
        first_name: data.firstName,
        last_name: data.lastName,
        email: data.email,
        mobile_number: data.mobileNumber,
        department: data.department,
        branch: data.branch
    });
}

var updateRegistrantCheckin = (id, isCheckin = true) => {
    
    isCheckin = isCheckin === true ? 1 : 0;
    
    return db(registrantTbl)
        .where("id", id)
        .update({
            checkin: isCheckin
        });
}

var getRegistrant = () => {
    return db(registrantTbl);
}

module.exports = {
    upsertRegistrant: upsertRegistrant,
    updateRegistrantCheckin: updateRegistrantCheckin,
    getRegistrant: getRegistrant
}