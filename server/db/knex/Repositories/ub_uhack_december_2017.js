'use strict'

var db = require("../db");

const uhacTbl = 'ub_uhack_dec_2017';

var upsertUhacParticipant = (data) => {

    var uhacRepo = new (require('./generic').Generic)(uhacTbl);

    return uhacRepo.upsertUsingMultiFields({
        first_name: data.firstName,
        last_name: data.lastName,
        email: data.email,
        mobile_number: data.mobileNumber,
        age: data.age,
        organization: data.organization,
        position: data.position,
        skill_type: data.skill_type,
        team: data.team
    }, {
        first_name: data.firstName,
        last_name: data.lastName,
        email: data.email,
        mobile_number: data.mobileNumber,
        age: data.age,
        organization: data.organization,
        position: data.position,
        skill_type: data.skill_type,
        team: data.team
    });
}

var updateUhacCheckin = (id, isCheckin = true) => {
    
    isCheckin = isCheckin === true ? 1 : 0;
    
    return db(uhacTbl)
        .where("id", id)
        .update({
            checkin: isCheckin
        });
}

var getParticipant = () => {
    return db(uhacTbl);
}

module.exports = {
    upsertUhacParticipant: upsertUhacParticipant,
    updateUhacCheckin: updateUhacCheckin,
    getParticipant: getParticipant
}