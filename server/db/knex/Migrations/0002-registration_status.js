exports.up = function (knex, Promise) {
    return knex.schema.table('ub_registrant', function (table) {
        table.boolean('checkin');
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.table('ub_registrant', function (table) {
        table.dropColumn('checkin');
    });
};