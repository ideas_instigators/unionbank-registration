exports.up = function(knex, Promise) {
	return knex.schema
	  
	//<issue>
	.createTable('ub_uhack_dec_2017', function(tbl)
	{
		//PK
		tbl.increments();
		
		//Fields
		tbl.string('first_name').notNullable();
    tbl.string('last_name').notNullable();
    tbl.string('email').notNullable();
    tbl.string('mobile_number').notNullable();
    tbl.string('age').notNullable();
    tbl.string('organization').notNullable();
    tbl.string('position').notNullable();
    tbl.string('skill_type').notNullable();
    tbl.string('team').nullable();
    tbl.boolean('checkin');
	});
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('ub_uhack_dec_2017');
};
