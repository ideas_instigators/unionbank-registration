exports.up = function(knex, Promise) {
	return knex.schema
	  
	//<issue>
	.createTable('ub_registrant', function(tbl)
	{
		//PK
		tbl.increments();
		
		//Fields
		tbl.string('first_name').notNullable();
        tbl.string('last_name').notNullable();
        tbl.string('email').notNullable();
        tbl.string('mobile_number').notNullable();
        tbl.string('department').notNullable();
        tbl.string('branch').notNullable();

	});
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('ub_registrant');
};
