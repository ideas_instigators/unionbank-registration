'use strict'

const uhacParticipantRepo = require('../knex/Repositories/ub_uhack_december_2017');

var addParticipant = (data) => {
    return uhacParticipantRepo.upsertUhacParticipant(data);
}

var updateUhacCheckin = (id, isCheckin) => {
    return uhacParticipantRepo.updateUhacCheckin(id, isCheckin);
}

var getParticipant = () => {
    return uhacParticipantRepo.getParticipant();
}

module.exports = {
    addParticipant: addParticipant,
    updateUhacCheckin: updateUhacCheckin,
    getParticipant: getParticipant
}