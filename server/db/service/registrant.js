'use strict'

const registrantRepo = require('../knex/Repositories/ub_registrant');

var addRegistrant = (data) => {
    return registrantRepo.upsertRegistrant(data);
}

var updateRegistrantCheckin = (id, isCheckin) => {
    return registrantRepo.updateRegistrantCheckin(id, isCheckin);
}

var getRegistrant = () => {
    return registrantRepo.getRegistrant();
}

module.exports = {
    addRegistrant: addRegistrant,
    updateRegistrantCheckin: updateRegistrantCheckin,
    getRegistrant: getRegistrant
}