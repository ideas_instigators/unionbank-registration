var express = require('express');
var errorHandler = require('errorhandler');
var bodyParser = require('body-parser');

var app = express();

var port = 3014;

app.set('views', './server/views');
app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

if (app.get('env') == 'development') {
    app.use(errorHandler());
}

app.use('/', require('./server/routes/index.js'));

app.listen(port, '0.0.0.0', function (err) {
    console.log('Running server on port ' + port);
});